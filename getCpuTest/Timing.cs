﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace getCpuTest
{
    public class Timing
    {

        private TimeSpan _duration;

        public Timing()
        {
            _duration = new TimeSpan(0);
        }

        public void StopTime()
        {
            _duration = Process.GetCurrentProcess().TotalProcessorTime;
        }

        public void StartTime()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();

        }

        public TimeSpan Result()
        {
            return _duration;
        }
    }
}
