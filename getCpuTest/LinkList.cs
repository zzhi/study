﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace getCpuTest
{
    public class LinkList<T> : IListDs<T>
    {

        private Node<T> head;

        /// <summary>
        /// 单链表头
        /// </summary>
        public Node<T> Head
        {
            get { return head; }
            set { head = value; }
        }

        public LinkList()
        {
            head = null;
        }

        /// <summary>
        /// 长度
        /// </summary>
        /// <returns></returns>
        public int GetLength()
        {
            Node<T> h = head;
            int len = 0;
            while (h != null)
            {
                h = h.Next;
                len++;
            }
            return len;
        }

        /// <summary>
        /// 清空
        /// </summary>
        public void Clear()
        {
            head = null;
        }

        /// <summary>
        /// 判断是否为空
        /// </summary>
        /// <returns></returns>
        public bool IsEmpty()
        {
            return head == null;
        }


        /// <summary>
        /// 末尾添加新元素
        /// </summary>
        /// <param name="item"></param>
        public void Append(T item)
        {
            Node<T> node = new Node<T>(item);
            if (head == null)
            {
                head = node;
            }
            else
            {
                Node<T> h = head;
                while (h.Next != null)
                {
                    h = h.Next;
                }
                h.Next = node;

            }
        }

        /// <summary>
        /// 插入
        /// </summary>
        /// <param name="item"></param>
        /// <param name="i"></param>
        public void Insert(T item, int i)
        {
            if (IsEmpty() || i < 1)
            {
                Console.WriteLine("链表为空或者位置错误");

            }
            else
            {
                if (i == 1)
                {
                    Node<T> node = new Node<T>(item);
                    node.Next = head;
                    head = node;
                }
                else
                {
                    Node<T> p = head;
                    Node<T> pre = new Node<T>();
                    int j = 1;
                    while (p.Next != null && j < i)
                    {
                        pre = p;
                        p = p.Next;
                        j++;
                    }

                    if (j == i)
                    {
                        Node<T> node = new Node<T>(item);
                        node.Next = p;
                        pre.Next = node;
                    }
                    else
                    {
                        Console.WriteLine("插入元素的位置不正确，无法执行插入操作，操作失败！");
                    }
                }
            }

        }

        /// <summary>
        /// 删除某个节点
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public T Delete(int i)
        {
            if (IsEmpty() || i < 1)
            {
                Console.WriteLine("链表为空或者位置错误");
                return default(T);
            }
            else
            {
                if (i == 1)
                {
                    Node<T> q = head;
                    head = head.Next;
                    return q.Data;
                }
                else
                {
                    Node<T> node = new Node<T>();
                    Node<T> h = head;
                    int j = 1;
                    while (h.Next != null && j < i)
                    {
                        node = h;
                        h = h.Next;
                        j++;
                    }

                    if (j == i)
                    {
                        node.Next = h.Next;
                        return h.Data;
                    }
                    else
                    {
                        Console.WriteLine("位置不正确");
                        return default(T);
                    }
                }
            }
        }


        // 获得单链表中第i个数据元素
        public T GetElem(int i)
        {
            // 定义要返回的元素，并赋初值
            T tmp = default(T);

            // 判断是否为空表
            if (IsEmpty())
            {
                Console.WriteLine("单链表表中不存在数据元素，无法执行获取操作，操作失败！");
            }
            // 判断用户指定的获取位置是否合理
            else if (i < 1)
            {
                Console.WriteLine("获取元素的位置不正确，无法执行获取操作，操作失败！");
            }
            // 执行获取操作，如果位置超过单链表长度，则获得到的为最后一个结点的值
            else
            {
                Node<T> p = new Node<T>();
                p = head;
                int j = 1;

                while (p.Next != null && j < i)
                {
                    p = p.Next;
                    j++;
                }

                tmp = p.Data;
            }

            // 返回被操作的元素（或默认值）
            return tmp;
        }


        public int Locate(T value)
        {
            // 定义要返回的索引，-1表示未找到或查找失败【注意：此处i表示是索引，而非位置！】
            int i;

            // 判断是否为空表
            if (IsEmpty())
            {
                Console.WriteLine("单链表表中不存在数据元素，无法执行查找操作，操作失败！");
                i = -1;
            }
                // 执行查找操作
            else
            {
                Node<T> p = new Node<T>();
                p = head;
                i = 0;
                while (!p.Data.Equals(value) && p.Next != null)
                {
                    p = p.Next;
                    i++;
                }
            }

            // 返回查找到的索引（或默认值）
            return i;
        }






    }
}
