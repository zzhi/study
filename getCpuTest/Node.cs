﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace getCpuTest
{
    public class Node<T>
    {
        public T Data { get;set; }
   
        public Node<T> Next { get;set;}
   
        public Node()
        {
            Data = default(T);
            Next = null;
        }

        public Node(T item)
        {
            Data = item;
            Next = null;
        }

        public Node(T item,Node<T> nNode)
        {
            Data = item;
            Next = nNode;
        }
    }
}
