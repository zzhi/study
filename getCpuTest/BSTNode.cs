﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace getCpuTest
{
    public class BstNode
    {
        public int Data;
        public BstNode Left;
        public BstNode Right;

        public void DisplayNode()
        {
            Console.Write(Data+" ");
        }
        public BstNode()
        {
            
        }
        public  BstNode(int i)
        {
            Data = i;
        }
    }
}
