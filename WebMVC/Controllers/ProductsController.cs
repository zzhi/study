﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using WebMVC.Models;

namespace WebMVC.Controllers
{
    public class ProductsController : Controller
    {
        private static List<Product> list;
        static int counter = 2;
        static ProductsController()
        {
            list = new List<Product>();
            list.Add(new Product { Id = 1, Name = "张三", Category="蔬菜",Price=(decimal) 12.3 });
            list.Add(new Product { Id = 2, Name = "李四", Category = "蔬菜", Price = (decimal) 12.3 });
        }
        // GET: Products
        public ActionResult Index()
        {
            return View("List", list);
        }

        [HttpGet]
        public ActionResult Create()
        {
            Product p = new Product();
            return View(p);
        }

        [HttpPost]
        public ActionResult Create(Product product)
        {
            Interlocked.Increment(ref counter);
            if (ModelState.IsValid)
            {
                product.Id = counter;
                list.Add(product);
            }
            return View("List",list);
        }

        
        public ActionResult Edit(int id)
        {
            Product pp = list.Where(p=>p.Id==id).FirstOrDefault();
           
            return View(pp);
        }

        [HttpPost]
        public ActionResult Edit(Product product)
        {
            Product pp = list.Where(p => p.Id == product.Id).FirstOrDefault();
            pp.Name = product.Name;
            pp.Price = product.Price;
            pp.Category = product.Category;

            return View("List", list);
        }



        public ActionResult Details(int id)
        {
            Product pp = list.Where(p => p.Id == id).FirstOrDefault();


            return View(pp);
        }

        public ActionResult Delete(int id)
        {
            Product pp = list.Where(p => p.Id == id).FirstOrDefault();
            list.Remove(pp);

            return View("List", list);
        }

    }
}