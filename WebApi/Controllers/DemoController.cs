﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Security;

namespace WebApi.Controllers
{
    [ApiAuth]
    public class DemoController : ApiController
    {
        public IEnumerable<string> GetUsers()
        {
            
            yield return HttpContext.Current.User.Identity.Name;
            yield return Thread.CurrentPrincipal.Identity.Name;
            yield return this.User.Identity.Name;



            var cookie = Request.Headers.GetCookies();

            FormsAuthenticationTicket ticket = null;

            foreach (var perCookie in cookie[0].Cookies)
            {
                if (perCookie.Name == FormsAuthentication.FormsCookieName)
                {
                    ticket = FormsAuthentication.Decrypt(perCookie.Value);
                    break;
                }
            }

            yield return ticket.Name;
        }
    }
}
