﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            RelativeSource rs = new RelativeSource(RelativeSourceMode.FindAncestor);
            rs.AncestorLevel = 2;
            rs.AncestorType = typeof (Grid);
            Binding b = new Binding("Name") {RelativeSource=rs};
            this.t1.SetBinding(TextBox.TextProperty,b);

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Human h = (Human)this.FindResource("human1");
            
            MessageBox.Show(h.Name+h.Child.Name);  
        }
    }
}
